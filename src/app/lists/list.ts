import { IItem } from "./item";

export interface IList {
    Id: number;
    Name: string;
    Description: string;
    Guid: string;
    ItemsList: IItem[];
}