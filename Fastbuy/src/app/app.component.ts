import { Component } from '@angular/core';
import { BackendService } from './lists/backend.service';

@Component({
  selector: 'app-root',
  template: `<div><h1>{{pageTitle}}</h1>
  <pm-list></pm-list></div>`,
  styleUrls: ['./app.component.css'],
  providers: [BackendService]
})
export class AppComponent {
  title = 'app';
}
