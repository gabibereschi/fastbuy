export interface IItem {
    Id: number;
    Name: string;
    Description: string;
    Guid: string;
    ListId: number;
}