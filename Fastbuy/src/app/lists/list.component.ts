import { OnInit, Component } from "@angular/core";
import { IItem } from "./item";
import { IList } from "./list";
import { BackendService } from "./backend.service"

@Component({
    selector: 'pm-list',
    templateUrl: './list.component.html'
})

export class ListComponent implements OnInit {

    pageTitle: string = 'Lists';
    panelOpenState: boolean = false;
    items: IItem[] = [];
    lists: IList[] = [];


    ngOnInit(): void {
        this._backendService.getLists()
            .subscribe(lists => {
                this.lists = lists;
            })
    }

    constructor(private _backendService: BackendService) {

    }

}