import { Injectable } from "@angular/core";
import { IList } from "./list";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../environments/environment';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';



@Injectable()
export class BackendService {
    private _baseUrl = environment.baseUrl;
    private _smarterAspUrl = environment.smarterAspUrl;

    constructor(private _http: HttpClient) { }

    getLists(): Observable<IList[]> {
        return this._http.get<IList[]>(this._smarterAspUrl + 'List/GetAll');
    }
}